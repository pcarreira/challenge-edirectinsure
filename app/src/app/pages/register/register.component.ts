import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})
export class RegisterComponent implements OnInit {

	form: FormGroup;
	invalidRegisterCode: string;
	loading = false;

  constructor(
    private fb: FormBuilder,
		private authService: AuthService,
		private router: Router
  ) {
  }

  async ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async onSubmit() {
		const { name, username, password } = this.form.value;
		try {
			this.loading = true;
			await this.authService.register(name, username, password);
			this.loading = false;
			this.router.navigate(['login']);
		} catch(e) {
			this.loading = false;
			this.invalidRegisterCode = e.error ? e.error.code : '';
		}
  }
}
