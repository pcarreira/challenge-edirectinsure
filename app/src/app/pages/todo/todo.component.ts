import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/model/project.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-todo',
	templateUrl: './todo.component.html',
	styleUrls: ['./todo.component.less']
})
export class TodoComponent implements OnInit {

	form: FormGroup;
	loading = false;
	newProjectError = false;
	projects = [];

	constructor(
		private fb: FormBuilder,
		private projectService: ProjectService,
		public translate: TranslateService
	) { }

	async ngOnInit() {
		this.form = this.fb.group({
			projectName: ['', Validators.required]
		});
		await this.loadProjects();
	}

	async loadProjects() {
		try {
			this.loading = true;
			this.projects = await this.projectService.list() || [];
			this.loading = false;
		} catch (e) {
			this.loading = false;
		}
	}

	async createProject() {
		const { projectName } = this.form.value;
		try {
			this.loading = true;
			await this.projectService.create(projectName);
			await this.loadProjects();
			this.loading = false;
		} catch (e) {
			this.newProjectError = e.error ? e.error.code : undefined;
			this.loading = false;
		}
	}

	deleteProject(project: Project) {
		this.projects = this.projects.filter(proj => proj.id !== project.id);
	}
}
