import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
	invalidRegisterCode: string;
	loading = false;

  constructor(
		private fb: FormBuilder,
		private authService: AuthService,
		private dataService: DataService,
		private router: Router
  ) {
  }

  async ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async onSubmit() {
		const { username, password } = this.form.value;
		try {
			this.loading = true;
			const login = await this.authService.login(username, password);
			this.dataService.saveToken(login.token);
			this.loading = false;
			this.router.navigate(['todo']);
		} catch(e) {
			this.loading = false;
			this.invalidRegisterCode = e.error ? e.error.code : '';
		}
  }
}