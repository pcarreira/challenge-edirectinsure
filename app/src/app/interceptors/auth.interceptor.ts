import { Injectable } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor,
	HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { catchError, switchMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

	private isRefreshing = false;

	constructor(public authService: AuthService, private dataService: DataService) { }

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		request = this.addToken(request, this.dataService.getToken());

		return next.handle(request)
			.pipe(
				catchError(error => {
					if (error instanceof HttpErrorResponse && error.status === 401) {
						return this.handle401Error(request, next, error);
					}
					throw error;
				})
			);
	}

	private addToken(request: HttpRequest<any>, token: string) {
		return request.clone({
			setHeaders: {
				'Authorization': `Bearer ${token}`
			}
		});
	}

	private handle401Error(request: HttpRequest<any>, next: HttpHandler, error) {
		if (!this.isRefreshing) {
			this.isRefreshing = true;
	
			return this.authService.refreshToken().pipe(
				switchMap((res: any) => {
					this.isRefreshing = false;
					this.dataService.saveToken(res.token);
					return next.handle(this.addToken(request, res.token));
				}));
	
		} else {
			this.isRefreshing = false;
			throw error;
		}
	}
}
