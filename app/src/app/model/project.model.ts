import { Task } from './task.model';

export interface Project {
	id: string,
	name: string,
	tasks: Task[]	
}