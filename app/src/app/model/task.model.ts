export interface Task {
	id: string,
	description: string,
	creationDate: Date,
	finishDate: Date,
	status: string
}