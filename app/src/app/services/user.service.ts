import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DataService } from './data.service';
import { MyProfile } from '../model/my-profile.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
		private http: HttpClient,
		private dataService: DataService
		) { }
	
	myprofile(): Promise<MyProfile> {
		return this.http.get<MyProfile>(`${environment.server}/myprofile`, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}
}
