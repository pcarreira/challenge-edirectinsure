import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from './data.service';
import { environment } from 'src/environments/environment';
import { Project } from '../model/project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

	constructor(
		private http: HttpClient,
		private dataService: DataService
	) { }

	create(name: string) {
		return this.http.post(`${environment.server}/project`, {
			name
		}, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}

	list(): Promise<Project[]> {
		return this.http.get<Project[]>(`${environment.server}/project`, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}

	refreshProject(id: string): Promise<Project> {
		return this.http.get<Project>(`${environment.server}/project/${id}`, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}

	addTask(projectId: string, description: string) {
		return this.http.post(`${environment.server}/project/${projectId}/task`, {
			description
		}, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}

	finishTask(projectId: string, taskId: string) {
		return this.http.post(`${environment.server}/project/${projectId}/task/${taskId}/finish`, {}, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}

	update(projectId: string, name: string) {
		return this.http.put(`${environment.server}/project/${projectId}`, {
			name
		}, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}

	delete(projectId: string) {
		return this.http.delete(`${environment.server}/project/${projectId}`, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}

	deleteTask(projectId: string, taskId: string) {
		return this.http.delete(`${environment.server}/project/${projectId}/task/${taskId}`, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}
}
