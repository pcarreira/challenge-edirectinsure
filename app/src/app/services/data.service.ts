import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
	private token: string;

	constructor() { 
		this.token = sessionStorage.getItem('token');
	}
	
	saveToken(token: string) {
		this.token = token;
		sessionStorage.setItem('token', token);
	}

	getToken() {
		return this.token;
	}

	cleanToken() {
		sessionStorage.removeItem('token');
		this.token = undefined;
	}
}
