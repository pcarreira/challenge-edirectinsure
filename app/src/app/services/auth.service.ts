import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LoginResponse } from '../model/login-response.model';
import { DataService } from './data.service';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(
		private http: HttpClient,
		private dataService: DataService
	) { }

	register(name: string, username: string, password: string) {
		return this.http.post(`${environment.server}/register`, {
			name,
			username,
			password
		}).toPromise();
	}

	login(username: string, password: string): Promise<LoginResponse> {
		return this.http.post<LoginResponse>(`${environment.server}/login`, {
			username,
			password
		}).toPromise();
	}

	logout(): Promise<LoginResponse> {
		return this.http.post<LoginResponse>(`${environment.server}/logout`, {}, {
			headers: {
				authorization: `Bearer ${this.dataService.getToken()}`
			}
		}).toPromise();
	}

	refreshToken(): Observable<LoginResponse> {
		return this.http.post<LoginResponse>(`${environment.server}/refreshToken`, {});
	}
}
