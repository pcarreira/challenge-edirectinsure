import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/services/data.service';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';
import { MyProfile } from 'src/app/model/my-profile.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {
	language = '';
	languages = [];
	loading = false;
	user: MyProfile;

  constructor(
		private router: Router,
		private authService: AuthService,
		private dataService: DataService,
		public translate: TranslateService,
		private userService: UserService
	) { }

  ngOnInit(): void {
		this.getProfile();
		this.updateLanguage();
	}

	async getProfile() {
		try {
			this.loading = true;
			this.user = await this.userService.myprofile();
			this.loading = false;
		} catch (e) {
			this.loading = false;
			this.router.navigate(['/']);
		}
	}
	
	async logout() {
		try {
			await this.authService.logout();
			this.dataService.cleanToken();
			this.router.navigate(['/']);
		} catch (e) {
			this.dataService.cleanToken();
			this.router.navigate(['/']);
		}
	}


	async changeLanguage(language: string) {
		await this.translate.use(language);
		this.updateLanguage();
	}

	updateLanguage() {
		this.language = this.translate.currentLang;
		this.languages = this.translate.getLangs();
		this.translate.onLangChange.subscribe(() => {
			this.language = this.translate.currentLang;
		});
	}
}
