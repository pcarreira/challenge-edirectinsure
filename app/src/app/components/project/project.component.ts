import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Project } from 'src/app/model/project.model';
import { ProjectService } from 'src/app/services/project.service';
import { Task } from 'src/app/model/task.model';

@Component({
	selector: 'app-project',
	templateUrl: './project.component.html',
	styleUrls: ['./project.component.less']
})
export class ProjectComponent implements OnInit {
	@Input() project;
	@Output() onDelete = new EventEmitter();
	
	loading = false;

	constructor(
		private projectService: ProjectService
	) { }

	ngOnInit(): void {
	}

	async refreshProject() {
		try {
			this.loading = true;
			this.project = await this.projectService.refreshProject(this.project.id);
			this.loading = false;
		}catch(e) {
			this.loading = false;
		}
	}

	async addTask(project: Project) {
		try {
			this.loading = true;
			await this.projectService.addTask(project.id, project['newTaskValue'])
			await this.refreshProject();
			this.loading = false;
		} catch (e) {
			this.loading = false;
		}
	}

	async selectTask(project: Project, task: Task) {
		try {
			this.loading = true;
			await this.projectService.finishTask(project.id, task.id)
			await this.refreshProject();
			this.loading = false;
		} catch (e) {
			this.loading = false;
		}
	}

	async editProject(project: Project) {
		try {
			this.loading = true;
			await this.projectService.update(project.id, project.name);
			await this.refreshProject();
			this.loading = false;
		} catch (e) {
			this.loading = false;
		}
	}

	async deleteProject(project: Project) {
		try {
			this.loading = true;
			await this.projectService.delete(project.id);
			this.onDelete.emit();
			this.loading = false;
		} catch (e) {
			this.loading = false;
		}
	}

	async deleteTask(project: Project, task: Task) {
		try {
			this.loading = true;
			await this.projectService.deleteTask(project.id, task.id);
			await this.refreshProject();
			this.loading = false;
		} catch (e) {
			this.loading = false;
		}
	}

}
