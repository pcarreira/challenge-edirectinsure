class ErrorResponse {
	constructor(status, code) {
		this.status = status;
		this.code = code;
	}

	send(res) {
		res
			.status(this.status)
			.send(this._build());
	}

	_build() {
		return {
			status: this.status,
			code: this.code
		};
	}
}

module.exports = ErrorResponse;