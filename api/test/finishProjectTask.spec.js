const chaiHttp = require('chai-http');
const chai = require('chai');

const app = require('../index');
const { User, Project } = require('../database');

chai.use(chaiHttp);

var expect = chai.expect;

describe('POST /project/:projectId/task/:taskId/finish', () => {
	before(async () => {
		await new User({
			_id: "5e8b790ff1c1241354029258",
			username: "test",
			password: "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
			name: "Test"
		}).save();

		await new Project({
			_id: "5e8b83aa0f2d5b1565ce8932",
			userId: "5e8b790ff1c1241354029258",
			name: "Test Project 1",
			tasks: [
				{
					_id: "5e8b910742e95b2920073ccf",
					description: "task 1",
					creationDate: "2020-04-06T20:28:55.462Z"
				}
			]
		}).save();
	});

	after(async () => {
		await User.deleteOne({ username: "test" });
		await Project.deleteOne({ name: "Test Project 1" });
	});

	it('should set as finish one project task', (done) => {
		chai
			.request(app)
			.post('/project/5e8b83aa0f2d5b1565ce8932/task/5e8b910742e95b2920073ccf/finish')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.JXU6lnCrKuTbCMuJm1xxwscEm2jp5j1Ix0U-3vHWQiw'
			})
			.end((err, res) => {
				expect(res.statusCode).to.eq(200);
				done();
			});
	});

	it('should send error for one invalid project task', (done) => {
		chai
			.request(app)
			.post('/project/5e8b83aa0f2d5b1565ce8932/task/5e8b910742e95b2920073cc3/finish')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.JXU6lnCrKuTbCMuJm1xxwscEm2jp5j1Ix0U-3vHWQiw'
			})
			.end((err, res) => {
				expect(res.statusCode).to.eq(404);
				done();
			});
	});
});