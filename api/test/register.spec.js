const chaiHttp = require('chai-http');
const chai = require('chai');

const app = require('../index');
const { User } = require('../database');

chai.use(chaiHttp);

var expect  = chai.expect;

describe('POST /register', () => {
	after(async () => {
		await User.deleteOne({ username: 'test' });
	});

  it('should create an user', (done) => {
    chai
      .request(app)
			.post('/register')
			.send({
				username: 'test',
				password: 'test',
				name: 'Test'
			})
      .end((err, res) => {
				expect(res.statusCode).to.eq(201);
        expect(res.body).to.deep.equal({});
        done();
      });
	});
	
	it('should not create an duplicated user', (done) => {
    chai
      .request(app)
			.post('/register')
			.send({
				username: 'test',
				password: 'test',
				name: 'Test'
			})
      .end((err, res) => {
				expect(res.statusCode).to.eq(400);
        expect(res.body).to.deep.equal({
					code: 'DUPLICATED_USER',
					status: 400
				});
        done();
      });
	});
	
	it('should send error for one invalid input schema', (done) => {
		chai
      .request(app)
			.post('/register')
			.send({})
      .end((err, res) => {
				expect(res.statusCode).to.eq(400);
				expect(res.body).to.be.deep.eq({
					code: "INVALID_SCHEMA",
					status: 400
				});
        done();
      });
	});
});