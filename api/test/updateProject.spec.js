const chaiHttp = require('chai-http');
const chai = require('chai');

const app = require('../index');
const { User, Project } = require('../database');

chai.use(chaiHttp);

var expect = chai.expect;

describe('PUT /project', () => {
	before(async () => {
		await new User({
			_id: "5e8b790ff1c1241354029258",
			username: "test",
			password: "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
			name: "Test"
		}).save();

		await new Project({
			_id : "5e8b83aa0f2d5b1565ce8932",
			userId : "5e8b790ff1c1241354029258",
			name : "Test Project"
		}).save();
	});

	after(async () => {
		await User.deleteOne({ username: "test" });
		await Project.deleteOne({ _id: "5e8b83aa0f2d5b1565ce8932" });
	});

	it('should update the project properties', (done) => {
		chai
			.request(app)
			.put('/project/5e8b83aa0f2d5b1565ce8932')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.JXU6lnCrKuTbCMuJm1xxwscEm2jp5j1Ix0U-3vHWQiw'
			})
			.send({
				name: "Updated Test Project"
			})
			.end((err, res) => {
				expect(res.statusCode).to.eq(200);
				done();
			});
	});

	it('should send error when the project id not exists', (done) => {
		chai
			.request(app)
			.put('/project/5e8b83aa0f2d5b1565ce8931')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.JXU6lnCrKuTbCMuJm1xxwscEm2jp5j1Ix0U-3vHWQiw'
			})
			.send({
				name: "Updated Test Project"
			})
			.end((err, res) => {
				expect(res.statusCode).to.eq(404);
				done();
			});
	});

	it('should send error for one invalid input schema', (done) => {
		chai
			.request(app)
			.put('/project/5e8b83aa0f2d5b1565ce8931')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.JXU6lnCrKuTbCMuJm1xxwscEm2jp5j1Ix0U-3vHWQiw'
			})
			.send({})
			.end((err, res) => {
				expect(res.statusCode).to.eq(400);
				expect(res.body).to.be.deep.eq({
					code: "INVALID_SCHEMA",
					status: 400
				});
				done();
			});
	});
});