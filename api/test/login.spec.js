const chaiHttp = require('chai-http');
const chai = require('chai');

const app = require('../index');
const { User } = require('../database');

chai.use(chaiHttp);

var expect  = chai.expect;

describe('POST /login', () => {
	before(async () => {
		await new User({
			_id : "5e8b790ff1c1241354029258",
			username : "test",
			password : "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
			name : "Test"
		}).save();
	});

	after(async () => {
		await User.deleteOne({ username : "test" });
	});

  it('should login with success', (done) => {
    chai
      .request(app)
			.post('/login')
			.send({
				username: 'test',
				password: 'test'
			})
      .end((err, res) => {
				expect(res.statusCode).to.eq(200);
        expect(res.body).to.have.own.property('token');
        done();
      });
	});

	it('should send 401 for an invalid username', (done) => {
    chai
      .request(app)
			.post('/login')
			.send({
				username: 'invalid',
				password: 'test'
			})
      .end((err, res) => {
				expect(res.statusCode).to.eq(401);
        done();
      });
	});

	it('should send 401 for an invalid password', (done) => {
    chai
      .request(app)
			.post('/login')
			.send({
				username: 'test',
				password: 'invalid'
			})
      .end((err, res) => {
				expect(res.statusCode).to.eq(401);
        done();
      });
	});

	it('should send 401 for an invalid username/password', (done) => {
    chai
      .request(app)
			.post('/login')
			.send({
				username: 'invalid',
				password: 'invalid'
			})
      .end((err, res) => {
				expect(res.statusCode).to.eq(401);
        done();
      });
	});

	it('should send error for one invalid input schema', (done) => {
		chai
      .request(app)
			.post('/login')
			.send({})
      .end((err, res) => {
				expect(res.statusCode).to.eq(400);
				expect(res.body).to.be.deep.eq({
					code: "INVALID_SCHEMA",
					status: 400
				});
        done();
      });
	});
});