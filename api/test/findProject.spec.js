const chaiHttp = require('chai-http');
const chai = require('chai');

const app = require('../index');
const { User, Project } = require('../database');

chai.use(chaiHttp);

var expect = chai.expect;

describe('GET /project/:id', () => {
	before(async () => {
		await new User({
			_id: "5e8b790ff1c1241354029258",
			username: "test",
			password: "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
			name: "Test"
		}).save();

		await new Project({
			_id: "5e8b83aa0f2d5b1565ce8932",
			userId: "5e8b790ff1c1241354029258",
			name: "Test Project 1"
		}).save();
	});

	after(async () => {
		await User.deleteMany({ username: { $in: ["test", "test2"] } });
		await Project.deleteMany({ _id: { $in: ["5e8b83aa0f2d5b1565ce8932"] } });
	});

	it('should find one project', (done) => {
		chai
			.request(app)
			.get('/project/5e8b83aa0f2d5b1565ce8932')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.JXU6lnCrKuTbCMuJm1xxwscEm2jp5j1Ix0U-3vHWQiw'
			})
			.end((err, res) => {
				expect(res.statusCode).to.eq(200);
				expect(res.body).to.be.deep.eq({
					id: "5e8b83aa0f2d5b1565ce8932",
					name: "Test Project 1",
					tasks: []
				});
				done();
			});
	});

	it('should return empty projects response', (done) => {
		chai
			.request(app)
			.get('/project/5e8b83aa0f2d5b1565ce8933')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTciLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.IKJ7TIAchtmi7COjApIE9cmePloLgaNb4BBYD2x37qY'
			})
			.end((err, res) => {
				expect(res.statusCode).to.eq(404);
				done();
			});
	});
});