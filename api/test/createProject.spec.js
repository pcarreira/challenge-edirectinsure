const chaiHttp = require('chai-http');
const chai = require('chai');

const app = require('../index');
const { User, Project } = require('../database');

chai.use(chaiHttp);

var expect = chai.expect;

describe('POST /project', () => {
	before(async () => {
		await new User({
			_id: "5e8b790ff1c1241354029258",
			username: "test",
			password: "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
			name: "Test"
		}).save();
	});

	after(async () => {
		await User.deleteOne({ username: "test" });
		await Project.deleteOne({ name: "Test Project" });
	});

	it('should create an project', (done) => {
		chai
			.request(app)
			.post('/project')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.JXU6lnCrKuTbCMuJm1xxwscEm2jp5j1Ix0U-3vHWQiw'
			})
			.send({
				name: "Test Project"
			})
			.end((err, res) => {
				expect(res.statusCode).to.eq(201);
				done();
			});
	});

	it('should send error for one invalid input schema', (done) => {
		chai
			.request(app)
			.post('/project')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6OTk5OTk5OTk5OX0.JXU6lnCrKuTbCMuJm1xxwscEm2jp5j1Ix0U-3vHWQiw'
			})
			.send({})
			.end((err, res) => {
				expect(res.statusCode).to.eq(400);
				expect(res.body).to.be.deep.eq({
					code: "INVALID_SCHEMA",
					status: 400
				});
				done();
			});
	});
});