const chaiHttp = require('chai-http');
const chai = require('chai');

const app = require('../index');
const { User } = require('../database');

chai.use(chaiHttp);

var expect  = chai.expect;

describe('POST /refreshToken', () => {
	before(async () => {
		await new User({
			_id : "5e8b790ff1c1241354029258",
			username : "test",
			password : "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08",
			name : "Test"
		}).save();
	});

	after(async () => {
		await User.deleteOne({ username : "test" });
	});

  it('should refresh the user token with success', (done) => {
    chai
      .request(app)
			.post('/refreshToken')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTgiLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6MX0.SaeAcU918JEM5fjAvmOblK7g8Ia7X0MPj-AKiT16Nds'
			})
      .end((err, res) => {
				expect(res.statusCode).to.eq(200);
        expect(res.body).to.have.own.property('token');
        done();
      });
	});

	it('should not refresh the user token for one invalid user', (done) => {
    chai
      .request(app)
			.post('/refreshToken')
			.set({
				authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZThiNzkwZmYxYzEyNDEzNTQwMjkyNTciLCJpYXQiOjE1ODYxOTk2ODcsImV4cCI6MX0._SfekL4A77oMnLhTpVLYVs6VPk7vSwsOr_ZEN132NbI'
			})
      .end((err, res) => {
				expect(res.statusCode).to.eq(401);
        done();
      });
	});
});