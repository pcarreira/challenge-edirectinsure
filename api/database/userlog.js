const mongoose = require('mongoose');

const schema = {
	userId: { 
		type: mongoose.Types.ObjectId, 
		require: true
	},
	timestamp: { 
		type: Date, 
		require: true
	},
	action: { 
		type: String, 
		require: true
	}
}

module.exports = mongoose.model('userlog', schema, 'userlog');
