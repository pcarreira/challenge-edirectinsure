const mongoose = require('mongoose');

function connect() {
	mongoose.connect(process.env.MONGO_DB, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useCreateIndex: true
	});
}

module.exports = connect;