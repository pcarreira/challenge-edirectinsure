const mongoose = require('mongoose');

const schema = {
	username: { 
		type: String, 
		require: true, 
		unique: true 
	},
	password: { 
		type: String, 
		require: true
	},
	name: { 
		type: String, 
		require: true
	}
}

module.exports = mongoose.model('user', schema, 'user');
