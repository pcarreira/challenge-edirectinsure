const mongoose = require('mongoose');

const taskSchema = mongoose.Schema({
	description: { 
		type: String, 
		require: true
	},
	creationDate: {
		type: Date,
		required: true
	},
	finishDate: {
		type: Date
	},
	status: { 
		type: String, 
		require: true
	}
})

const schema = {
	userId: { 
		type: mongoose.Types.ObjectId, 
		require: true
	},
	name: { 
		type: String, 
		require: true
	},
	tasks: [taskSchema]
}

module.exports = mongoose.model('project', schema, 'project');