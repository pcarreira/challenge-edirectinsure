const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config()

const { register, login, myprofile, logout, createProject, listProject, updateProject, deleteProject, createProjectTask, finshProjectTask, deleteProjectTask, getProject, refreshToken } = require('./controller');
const database = require('./database');
const authMiddleware = require('./middleware/authMiddleware');

database.connect();
const app = express();
const port = process.env.PORT;

app.use(bodyParser.json());

app.post('/register', register);
app.post('/login', login);
app.post('/logout', authMiddleware, logout);
app.post('/refreshToken', refreshToken);

app.get('/myprofile', authMiddleware, myprofile);

app.post('/project', authMiddleware, createProject);
app.get('/project', authMiddleware, listProject);
app.get('/project/:id', authMiddleware, getProject);
app.put('/project/:id', authMiddleware, updateProject);
app.delete('/project/:id', authMiddleware, deleteProject);

app.post('/project/:id/task', authMiddleware, createProjectTask);
app.post('/project/:projectId/task/:taskId/finish', authMiddleware, finshProjectTask);
app.delete('/project/:projectId/task/:taskId', authMiddleware, deleteProjectTask);

app.listen(port, () => {
	console.log(`API listening at http://localhost:${port}`);
});

module.exports = app;