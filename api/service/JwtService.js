const jwt = require('jsonwebtoken');

class JwtService {
	generate(userId) {
		return jwt.sign({ userId }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_EXPIRATION_TIME });
	}
}

module.exports = JwtService;