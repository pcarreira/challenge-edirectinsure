const { Project } = require('../database');

class ProjectService {
	create(userId, name) {
		return new Project({
			userId,
			name
		}).save();
	}

	list(userId) {
		return Project.find({ userId });
	}

	find(userId, projectId) {
		return Project.findOne({ _id: projectId, userId });
	}

	updateName(projectId, userId, name) {
		return Project.updateOne({ _id: projectId, userId }, { $set: { name } });
	}

	delete(projectId, userId) {
		return Project.deleteOne({ _id: projectId, userId });
	}
}

module.exports = ProjectService;