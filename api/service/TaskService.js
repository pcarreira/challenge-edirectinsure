const { Project } = require('../database');

class TaskService {
	create(userId, projectId, description) {
		return Project.updateOne({ _id: projectId, userId }, {
			$addToSet: {
				tasks: {
					description,
					creationDate: new Date(),
					status: "OPEN"
				}
			}
		});
	}

	finish(userId, projectId, taskId) {
		return Project.updateOne({ _id: projectId, userId, 'tasks._id': taskId }, {
			$set: {
				"tasks.$.finishDate": new Date(),
				"tasks.$.status": "FINISHED",
			}
		});
	}

	delete(userId, projectId, taskId) {
		return Project.updateOne({ _id: projectId, userId }, {
			$pull: {
				tasks: {
					_id: taskId
				}
			}
		});
	}
}

module.exports = TaskService;