const sha256 = require('sha256');

const { User } = require('../database');
const { UserLog } = require('../database');

class UserService {
	create(username, password, name) {
		return new User({
			username, 
			password: sha256(password), 
			name
		}).save();
	}

	getUser(username, password) {
		return User.findOne({
			username, 
			password: sha256(password)
		});
	}

	getUserById(userId) {
		return User.findById(userId);
	}

	login(userId) {
		return new UserLog({
			userId,
			timestamp: new Date(),
			action: 'LOGIN'
		}).save();
	}

	refreshToken(userId) {
		return new UserLog({
			userId,
			timestamp: new Date(),
			action: 'REFRESH_TOKEN'
		}).save();
	}

	logout(userId) {
		return new UserLog({
			userId,
			timestamp: new Date(),
			action: 'LOGOUT'
		}).save();
	}
}

module.exports = UserService;