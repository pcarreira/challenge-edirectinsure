const jwt = require('jsonwebtoken');
const httpStatus = require('http-status-codes');

function checkToken(req, res, next) {
	const { authorization } = req.headers;
	const token = authorization.replace('Bearer ', '');
	try {
		var decoded = jwt.verify(token, process.env.JWT_SECRET);
		req.headers['app-user-id'] = decoded.userId;
		next();
	} catch(err) {
		res.status(httpStatus.UNAUTHORIZED).send();
	}
}

module.exports = checkToken;