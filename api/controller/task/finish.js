const httpStatus = require('http-status-codes');

const TaskService = require('../../service/TaskService');
const ErrorResponse = require('../../model/ErrorResponse');

async function finishProjectTask(req, res) {
	const { projectId, taskId } = req.params;
	const userId = req.headers['app-user-id'];
	const service = new TaskService();
	try {
		const result = await service.finish(userId, projectId, taskId);
		return result.n > 0 ? res.status(httpStatus.OK).send() : res.status(httpStatus.NOT_FOUND).send();
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'CREATE_PROJECT_ERROR');
		return response.send(res);
	}
}

module.exports = finishProjectTask;