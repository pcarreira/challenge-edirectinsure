const httpStatus = require('http-status-codes');

const TaskService = require('../../service/TaskService');
const ErrorResponse = require('../../model/ErrorResponse');

async function createProjectTask(req, res) {
	const { id } = req.params;
	const { description } = req.body;
	if(!description) {
		const response = new ErrorResponse(httpStatus.BAD_REQUEST, 'INVALID_SCHEMA');
		return response.send(res);
	}

	const userId = req.headers['app-user-id'];
	const service = new TaskService();
	try {
		await service.create(userId, id, description);
		return res.status(httpStatus.CREATED).send();
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'CREATE_PROJECT_TASK_ERROR');
		return response.send(res);
	}
}

module.exports = createProjectTask;