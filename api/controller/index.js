const register = require('./user/register');
const login = require('./user/login');
const myprofile = require('./user/myprofile');
const logout = require('./user/logout');
const createProject = require('./project/create');
const listProject = require('./project/list');
const updateProject = require('./project/update');
const deleteProject = require('./project/delete');
const createProjectTask = require('./task/create');
const finshProjectTask = require('./task/finish');
const deleteProjectTask = require('./task/delete');
const getProject = require('./project/get');
const refreshToken = require('./user/refreshToken');

module.exports = {
	register,
	login,
	logout,
	myprofile,
	createProject,
	listProject,
	updateProject,
	deleteProject,
	createProjectTask,
	finshProjectTask,
	deleteProjectTask,
	getProject,
	refreshToken
}