const httpStatus = require('http-status-codes');

const ProjectService = require('../../service/ProjectService');
const ErrorResponse = require('../../model/ErrorResponse');

async function deleteProject(req, res) {
	const { id } = req.params;
	const userId = req.headers['app-user-id'];
	const service = new ProjectService();
	try {
		const result = await service.delete(id, userId);
		return result.n > 0 ? res.status(httpStatus.OK).send() : res.status(httpStatus.NOT_FOUND).send();
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'CREATE_PROJECT_ERROR');
		return response.send(res);
	}
}

module.exports = deleteProject;