const httpStatus = require('http-status-codes');

const ProjectService = require('../../service/ProjectService');
const ErrorResponse = require('../../model/ErrorResponse');

async function updateProject(req, res) {
	const { name } = req.body;
	const { id } = req.params;
	if(!name) {
		const response = new ErrorResponse(httpStatus.BAD_REQUEST, 'INVALID_SCHEMA');
		return response.send(res);
	}

	const userId = req.headers['app-user-id'];
	const service = new ProjectService();
	try {
		const result = await service.updateName(id, userId, name);
		return result.n > 0 ? res.status(httpStatus.OK).send() : res.status(httpStatus.NOT_FOUND).send();
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'CREATE_PROJECT_ERROR');
		return response.send(res);
	}
}

module.exports = updateProject;