const httpStatus = require('http-status-codes');

const ProjectService = require('../../service/ProjectService');
const ErrorResponse = require('../../model/ErrorResponse');

async function listProject(req, res) {
	const userId = req.headers['app-user-id'];
	const service = new ProjectService();
	try {
		const projects = await service.list(userId);
		const mappedProjects = projects.map(p => ({
			id: p._id,
			name: p.name,
			tasks: p.tasks.map(task => ({
				id: task._id,
				description: task.description,
				creationDate: task.creationDate,
				finishDate: task.finishDate,
				status: task.status
			}))
		}))
		if(mappedProjects.length === 0) {
			return res.status(httpStatus.NO_CONTENT).send();
		}
		return res.status(httpStatus.OK).send(mappedProjects);
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'LIST_PROJECT_ERROR');
		return response.send(res);
	}
}

module.exports = listProject;