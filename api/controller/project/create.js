const httpStatus = require('http-status-codes');

const ProjectService = require('../../service/ProjectService');
const ErrorResponse = require('../../model/ErrorResponse');

async function createProject(req, res) {
	const { name } = req.body;
	if(!name) {
		const response = new ErrorResponse(httpStatus.BAD_REQUEST, 'INVALID_SCHEMA');
		return response.send(res);
	}

	const userId = req.headers['app-user-id'];
	const service = new ProjectService();
	try {
		await service.create(userId, name);
		return res.status(httpStatus.CREATED).send();
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'CREATE_PROJECT_ERROR');
		return response.send(res);
	}
}

module.exports = createProject;