const httpStatus = require('http-status-codes');

const ProjectService = require('../../service/ProjectService');
const ErrorResponse = require('../../model/ErrorResponse');

async function findProject(req, res) {
	const userId = req.headers['app-user-id'];
	const { id } = req.params;
	const service = new ProjectService();
	try {
		const project = await service.find(userId, id);
		if(!project) {
			return res.status(httpStatus.NOT_FOUND).send();
		}
		const mappedProject = {
			id: project._id,
			name: project.name,
			tasks: project.tasks.map(task => ({
				id: task._id,
				description: task.description,
				creationDate: task.creationDate,
				finishDate: task.finishDate,
				status: task.status
			}))
		};
		return res.status(httpStatus.OK).send(mappedProject);
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'FIND_PROJECT_ERROR');
		return response.send(res);
	}
}

module.exports = findProject;