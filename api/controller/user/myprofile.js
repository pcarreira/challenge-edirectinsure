const httpStatus = require('http-status-codes');

const UserService = require('../../service/UserService');
const ErrorResponse = require('../../model/ErrorResponse');

async function myProfile(req, res) {
	const userId = req.headers['app-user-id'];
	const service = new UserService();
	try {
		const user = await service.getUserById(userId);
		return res.status(httpStatus.OK).send({
			username: user.username,
			name: user.name
		});
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'MY_PROFILE_ERROR');
		return response.send(res);
	}
}

module.exports = myProfile;