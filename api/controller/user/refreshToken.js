const httpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');

const UserService = require('../../service/UserService');
const JwtService = require('../../service/JwtService');
const ErrorResponse = require('../../model/ErrorResponse');

async function refreshToken(req, res) {
	const { authorization } = req.headers;
	const token = authorization.replace('Bearer ', '');

	const service = new UserService();
	try {
		var decoded = jwt.verify(token, process.env.JWT_SECRET, { ignoreExpiration: true });
		const user = await service.getUserById(decoded.userId);
		if(!user) {
			return res.status(httpStatus.UNAUTHORIZED).send();
		}
		const newToken = new JwtService().generate(user._id);
		await service.refreshToken(user._id);
		return res.status(httpStatus.OK).send({ token: newToken });
	} catch(err) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'REFRESH_TOKEN_ERROR');
		return response.send(res);
	}
}

module.exports = refreshToken;