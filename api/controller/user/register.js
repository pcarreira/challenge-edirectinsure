const httpStatus = require('http-status-codes');

const UserService = require('../../service/UserService');
const ErrorResponse = require('../../model/ErrorResponse');

async function register(req, res) {
	const { username, password, name } = req.body;
	if(!username || !password || !name) {
		const response = new ErrorResponse(httpStatus.BAD_REQUEST, 'INVALID_SCHEMA');
		return response.send(res);
	}

	const service = new UserService();
	try {
		await service.create(username, password, name);
		return res.status(httpStatus.CREATED).send();
	} catch (error) {
		if(error.code === 11000) {
			const response = new ErrorResponse(httpStatus.BAD_REQUEST, 'DUPLICATED_USER');
			return response.send(res);
		}
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'CREATE_USER_ERROR');
		return response.send(res);
	}
}

module.exports = register;