const httpStatus = require('http-status-codes');

const UserService = require('../../service/UserService');
const ErrorResponse = require('../../model/ErrorResponse');

async function logout(req, res) {
	const userId = req.headers['app-user-id'];
	const service = new UserService();
	try {
		await service.logout(userId);
		return res.status(httpStatus.NO_CONTENT).send();
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'LOGOUT_ERROR');
		return response.send(res);
	}
}

module.exports = logout;