const httpStatus = require('http-status-codes');

const UserService = require('../../service/UserService');
const JwtService = require('../../service/JwtService');
const ErrorResponse = require('../../model/ErrorResponse');

async function login(req, res) {
	const { username, password } = req.body;
	if(!username || !password) {
		const response = new ErrorResponse(httpStatus.BAD_REQUEST, 'INVALID_SCHEMA');
		return response.send(res);
	}

	const service = new UserService();
	try {
		const user = await service.getUser(username, password);
		if(!user) {
			const error = new ErrorResponse(httpStatus.BAD_REQUEST, 'INVALID_USER_PASSWORD');
			return error.send(res);
		}
		const token = new JwtService().generate(user._id);
		await service.login(user._id);
		return res.status(httpStatus.OK).send({ token });
	} catch (error) {
		const response = new ErrorResponse(httpStatus.INTERNAL_SERVER_ERROR, 'LOGIN_ERROR');
		return response.send(res);
	}
}

module.exports = login;